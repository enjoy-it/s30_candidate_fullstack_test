<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $search = $request->input('search');

        $users = User::whereHas('roles', function ($query) use ($search) {
            $query->where('name', $search);
        })->get();

        return $this->responseWithSuccess(
            'Users have been found',
            Response::HTTP_OK,
            UserResource::collection($users)
        );
    }

    /**
     * @param StoreUserRequest $request
     * @return JsonResponse
     */
    public function store(StoreUserRequest $request): JsonResponse
    {
        $user = $this->userService->create($request);

        if ($user == null)
        {
            return $this->responseWithError(
                'There was an error during creating the user',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return $this->responseWithSuccess(
            'User has been created',
            Response::HTTP_CREATED
        );
    }
}
