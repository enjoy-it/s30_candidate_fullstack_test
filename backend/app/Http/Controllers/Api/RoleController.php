<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $roles = Role::all();

        return $this->responseWithSuccess(
            'Roles have been found',
            Response::HTTP_OK,
            RoleResource::collection($roles)
        );
    }
}
