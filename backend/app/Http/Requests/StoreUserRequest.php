<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullName' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email'],
            'roles' => ['required', 'array'],
            'roles.*' => ['required', 'string', 'distinct', 'exists:roles,name']
        ];
    }

    /**
     * @param Validator $validator
     * @return void
     */
    public function failedValidation(Validator $validator)
    {
       throw new HttpResponseException(response()->json([
         'success' => false,
         'message' => 'Validation errors',
         'data' => $validator->errors()
       ]));
    }

    /**
     * @return string[]
     */
    public function messages()
    {
        return [
            'fullName.required' => 'Full name is required',
            'email.required' => 'Email is required',
            'email.email' => 'Email is not correct',
            'email.unique' => 'Email is taken',
            'roles.required' => 'Role is required',
            'roles.*.exists' => 'Role does not exist'
        ];
     }
}
