<?php

namespace App\Services;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserService
{
    /**
     * @param StoreUserRequest $request
     * @return null
     */
    public function create(StoreUserRequest $request)
    {
        $user = null;

        DB::beginTransaction();
        try
        {
            $user = User::create([
                'fullName' => $request->fullName,
                'email' => $request->email
            ]);

            $user->assignRole([$request->roles]);

            DB::commit();
        }
        catch (\Exception $exception)
        {
            DB::rollback();
        }

        return $user;
    }
}
