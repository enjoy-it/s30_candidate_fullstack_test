<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Spatie\Permission\Models\Role;

/**
 * @extends Factory
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'fullName' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }

    /**
     * @return UserFactory
     */
    public function configure(): UserFactory
    {
        return $this->afterCreating(function (User $user) {
            $listOfRolesNames = Role::get('name')->toArray();
            $countOfRoles = Role::count();
            $randomRoleName = $listOfRolesNames[rand(0, $countOfRoles - 1)]['name'];
            $user->assignRole($randomRoleName);
        });
    }
}
