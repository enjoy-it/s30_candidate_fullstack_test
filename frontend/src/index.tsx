import { ToastContainer } from 'react-toastify';
import reportWebVitals from './reportWebVitals';
import 'react-toastify/dist/ReactToastify.css';
import ReactDOM from 'react-dom';
import Router from './router';
import React from 'react';
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <ToastContainer />
    <Router />
  </React.StrictMode>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
