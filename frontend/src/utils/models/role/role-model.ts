export interface IRole {
  name: string;
}

export interface IRoleQuery {
  role: string;
}
