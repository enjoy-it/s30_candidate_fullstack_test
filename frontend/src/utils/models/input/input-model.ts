import { ChangeEventHandler } from 'react';

export interface IInput {
  handleInput: ChangeEventHandler<HTMLInputElement>;
  name: string;
  label: string;
  type: string;
}

export interface ISelect {
  handleSelect: ChangeEventHandler<HTMLSelectElement>;
  label: string;
  name: string;
  children: JSX.Element[] | undefined;
}
