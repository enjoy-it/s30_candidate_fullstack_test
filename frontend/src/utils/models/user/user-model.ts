import { IRole } from 'utils';

export interface IUser {
  fullName: string;
  email: string;
  roles: IRole[];
  createdAt: Date;
}

export interface IUserResponse {
  status: boolean;
  message: string;
  data: IUser[];
}

export interface IUserPost {
  fullName: string;
  email: string;
  roles: string[];
}
