export interface ICreateResponseData {
  email?: string[];
  roles?: string[];
  fullName?: string[];
}

export interface ICreateResponse {
  data: ICreateResponseData;
  message: string;
  success: boolean;
}
