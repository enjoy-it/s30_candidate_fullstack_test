import { IUser } from 'utils';
import ApiHandler from 'api';

export const getUsers = async (role: string): Promise<IUser[]> => {
  const { data } = await ApiHandler.get(`users?search=${role}`);
  return data;
};
