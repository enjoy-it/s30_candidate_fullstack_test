import ApiHandler from 'api';
import { toast } from 'react-toastify';
import { ICreateResponse, IUserPost } from 'utils';

export const createUser = async (values: IUserPost) => {
  const response: ICreateResponse = await ApiHandler.post('users', values);

  if (response.success) {
    toast.success(response.message);
  }

  return response;
};
