import { IRole } from 'utils';
import ApiHandler from 'api';

export const getRoles = async (): Promise<IRole[]> => {
  const { data } = await ApiHandler.get('roles');
  return data;
};
