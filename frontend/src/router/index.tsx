import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home, UserForm } from 'pages';

function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="create" element={<UserForm />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Router;
