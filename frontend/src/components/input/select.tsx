import { ISelect } from 'utils';

export const Select = ({ handleSelect, name, label, children }: ISelect) => {
  return (
    <label className="block text-left">
      <span className="text-black-700">{label}</span>
      <select name={name} id={name} className="form-multiselect border-2 border-gray-600 block w-full mt-1" multiple={true} onChange={handleSelect}>
        {children}
      </select>
    </label>
  );
};
