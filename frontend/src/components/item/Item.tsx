import { IUser } from 'utils';

export const Item = ({ fullName, email, roles, createdAt }: IUser) => {
  return (
    <>
      <tr className="whitespace-nowrap">
        <td className="px-6 py-4">
          <div className="text-sm text-gray-900">{fullName}</div>
        </td>
        <td className="px-6 py-4">
          <div className="text-sm text-gray-500">{email}</div>
        </td>
        <td className="px-6 py-4 text-sm text-gray-500">{roles.join(', ')}</td>
        <td className="px-6 py-4 text-sm text-gray-500">{createdAt}</td>
      </tr>
    </>
  );
};
