import React, { useState } from 'react';

export const useForm = <T>(initialState: T, callback?: any) => {
  const [values, setValues] = useState<T>(initialState);

  const onChange = (event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const onSelect = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setValues({ ...values, [event.target.name]: [event.target.value] });
  };

  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    await callback();
  };

  return {
    onChange,
    onSelect,
    onSubmit,
    values,
  };
};
