import { getRoles } from 'services';
import { useEffect, useState } from 'react';
import { IRole } from 'utils';

export const useGetRoles = () => {
  const [roles, setRoles] = useState<IRole[] | null>(null);

  useEffect(() => {
    const handleRoles = async () => {
      const fetchRoles = await getRoles();
      setRoles(fetchRoles);
    };

    handleRoles();
  }, []);

  return [roles];
};
