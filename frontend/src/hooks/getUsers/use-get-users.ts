import { getUsers } from 'services';
import { useEffect, useState } from 'react';
import { IRoleQuery, IUser } from 'utils';

export const useGetUsers = ({ role }: IRoleQuery) => {
  const [users, setUsers] = useState<IUser[] | null>(null);

  useEffect(() => {
    const handleUsers = async () => {
      const fetchUsers = await getUsers(role);
      setUsers(fetchUsers);
    };

    handleUsers();
  }, [role]);

  return [users];
};
