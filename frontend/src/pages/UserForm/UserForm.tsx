import { useGetRoles, useForm } from 'hooks';
import { createUser } from 'services';
import { IUserPost } from 'utils';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Input, Select } from 'components';

export const UserForm = () => {
  const [roles] = useGetRoles();
  const navigate = useNavigate();

  const initialState: IUserPost = {
    email: '',
    fullName: '',
    roles: [],
  };

  const { onChange, onSelect, onSubmit, values } = useForm(initialState, createUserCallback);

  async function createUserCallback() {
    const response = await createUser(values);

    if (response.success) {
      navigate('/');
    } else {
      if (response.data.email) {
        toast.error(response.data.email[0]);
      }

      if (response.data.roles) {
        toast.error(response.data.roles[0]);
      }

      if (response.data.fullName) {
        toast.error(response.data.fullName[0]);
      }
    }
  }

  return (
    <>
      <div className="h-screen flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <div className='<div className="max-w-md w-96 space-y-8">'>
          <form onSubmit={onSubmit}>
            <Input name="fullName" type="text" handleInput={onChange} label="Full name" />
            <Input name="email" type="email" handleInput={onChange} label="Email" />
            <Select name="roles" label="Roles" handleSelect={onSelect}>
              {roles?.map((role) => (
                <option key={role.name}>{role.name}</option>
              ))}
            </Select>
            <button
              type="submit"
              className="mt-6 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  );
};
