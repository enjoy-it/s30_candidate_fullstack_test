import { Item, Select } from 'components';
import { useGetUsers, useForm, useGetRoles } from 'hooks';
import { Link } from 'react-router-dom';
import { IRoleQuery, IUser } from 'utils';

export const Home = () => {
  const initialState: IRoleQuery = {
    role: 'Editor',
  };
  const [roles] = useGetRoles();
  const { onChange, values } = useForm(initialState);
  const [users] = useGetUsers(values);

  return (
    <>
      <div className="flex justify-center mx-auto">
        <div className="mr-4">
          <Select name="role" label="Roles" handleSelect={onChange}>
            {roles?.map((role) => (
              <option key={role.name}>{role.name}</option>
            ))}
          </Select>
          <Link to="/create">
            <button
              className="mt-6 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              type="button"
            >
              Create user!
            </button>
          </Link>
        </div>
        <div className="flex flex-col">
          <div className="w-full">
            <div className="border-b border-gray-200 shadow">
              <table className="divide-y divide-gray-300 ">
                <thead className="bg-gray-50">
                  <tr>
                    <th className="px-6 py-2 text-xs text-gray-500">Full name</th>
                    <th className="px-6 py-2 text-xs text-gray-500">Email</th>
                    <th className="px-6 py-2 text-xs text-gray-500">Roles</th>
                    <th className="px-6 py-2 text-xs text-gray-500">Created at</th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-300">
                  {users?.map((user: IUser) => (
                    <Item key={user.email} {...user} />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
