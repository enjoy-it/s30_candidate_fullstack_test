# recruitment
## Usage Backend

1. Clone repo

`git clone https://github.com/adamsmichal/recruitment.git`

2. Open backend directory

`cd backend`

3. Create .env

`cp .env.example .env`

4. Install dependencies

`composer install`

5. Run sail

`./vendor/bin/sail up`

6. Run migration with seeder

`./vendor/bin/sail artisan migrate:fresh --seed`

## Usage Frontend

1. Open Frontend directory

`cd fronted`

2. Install dependencies

`npm install`

3. Create .env

`cp .env.example .env`

4. Start server

`npm start`
